FROM ubuntu:latest
RUN apt-get update -y && apt-get upgrade -y
RUN apt install software-properties-common -y
RUN add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get install python3-pip -y && apt-get install python3-venv -y
RUN mkdir -p /home/google_maps_api
WORKDIR /home/google_maps_api
COPY ./logs ./logs
COPY ./tests ./tests
COPY ./utils ./utils
COPY ./conftest.py .
COPY ./requirements.txt .
RUN pip3 install -r requirements.txt
WORKDIR /home/google_maps_api/tests
RUN export PYTHONPATH=..
CMD pytest -s -v && bash